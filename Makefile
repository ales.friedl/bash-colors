.PHONY: default all build install clean clean_build clean_install

LIB = colors

default: all

all: build

build: build/lib/${LIB}

build/lib/%: src/lib/%.sh
	@mkdir -p ${@D}
	@cp $< $@

install: bin/lib/${LIB}

bin/lib/%: build/lib/%
	@mkdir -p ${@D}
	@cp $< $@

clean: clean_build clean_install

clean_build:
	@rm -fr build

clean_install:
	@rm -fr bin

