#!/bin/bash

# Black       0;30     Dark Gray     1;30
# Blue        0;34     Light Blue    1;34
# Green       0;32     Light Green   1;32
# Cyan        0;36     Light Cyan    1;36
# Red         0;31     Light Red     1;31
# Purple      0;35     Light Purple  1;35
# Brown       0;33     Yellow        1;33
# Light Gray  0;37     White         1;37
# Bold        1        Underscore    4
# Reverse     7        Normal        0

if [[ "$__COLORS__" == "" ]]; then

__COLORS__=true

function setup_colors() {
	if $1; then
		    black="\\033[00;30m";    darkgray="\\033[1;30m"
		     blue="\\033[00;34m";   lightblue="\\033[1;34m"
		    green="\\033[00;32m";  lightgreen="\\033[1;32m"
		     cyan="\\033[00;36m";   lightcyan="\\033[1;36m"
		      red="\\033[00;31m";    lightred="\\033[1;31m"
		   purple="\\033[00;35m"; lightpurple="\\033[1;35m"
		    brown="\\033[00;33m";      yellow="\\033[1;33m"
		lightgray="\\033[00;37m";       white="\\033[1;37m"
		     bold="\\033[1m";      underscore="\\033[4m"
		  reverse="\\033[7m";           cnone="\\033[00;00m"
		      cok="$lightgreen";         cko="$lightred"
		      cna="$white";
		    cwarn="$yellow";           cinfo="$white"
	else
		    black="";    darkgray=""
		     blue="";   lightblue=""
		    green="";  lightgreen=""
		     cyan="";   lightcyan=""
		      red="";    lightred=""
		   purple=""; lightpurple=""
		    brown="";      yellow=""
		lightgray="";       white=""
		     bold="";  underscore=""
		  reverse="";       cnone=""
		      cok="";         cko=""
		      cna="";
		    cwarn="";       cinfo=""
	fi
}

fi
